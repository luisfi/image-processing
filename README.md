Procesamiento de imagenes con OpenCV
====================================

Ejemplos para aprender a utilizar OpenCV. Se encuentran los
siguientes apartados:

* [I/O Básico](io-basico.ipynb)
  
  Se muestran las operaciones básicas de lectura, escritura y
  modificación de imágenes.

* [Matrices, colores y filtros](matrices.ipynb)

  Se muestra el manejo de matrices, así como la manipulación de sus
  elementos.

* [Segmentación y contornos](segmentacion.ipynb)

  Se aplican algoritmos para binarizar una imagen y para poder
  extraer características de éstas, como bordes y contornos.

* [Detección de objetos](deteccion-objetos.ipynb)

  Se utilizan diversos algoritmos para la detección de figuras
  simples como líneas hasta mas complejas como rostros, utilizando
  técnicas simples  y también algunas aplicaciones de aprendizaje
  de máquina.

Se está tomando como base el libro: 

OpenCV 3 Computer Vision with Python Cookbook de Alexey Spizhevoy
y Aleksandr Rybnikov.
